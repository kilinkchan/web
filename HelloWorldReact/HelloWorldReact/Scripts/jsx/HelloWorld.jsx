﻿var CommentBox = React.createClass({
    render: function () {
        return (
            <div className="commentBox">
                Hello, world! My name is KK
          </div>
        );
    }
});

ReactDOM.render(
    <CommentBox />,
    document.getElementById('content')
);

var CommentBox2 = React.createClass({
    render: function () {
        return (
            <div className="commentBox">
                Hello, world! My name is KK2
          </div>
        );
    }
});

ReactDOM.render(
    <CommentBox2 />,
    document.getElementById('content2')
);

var App2 = React.createClass({
    loadCommentsFromServer: function () {
        $.ajax({
            url: this.props.url,
            dataType: "json",
            success: function (data) {
                this.setState({ data: data });
            }.bind(this)
        });
    },    
    getInitialState: function () {
        return { data: '', test: 'initAAA' };
    },
    componentWillMount: function () {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render: function () {
        return (
            <div className="commentBox">
                <h1>{this.state.data}</h1>
            </div>
        );
    }
});

ReactDOM.render(
    <App2 url="/Home/GetDate" pollInterval={10000} />,
    document.getElementById('divDate')
);


var Test = React.createClass({
    handleCommentSubmit: function (comment) {
        alert(this.state.count)        
    },
    getInitialState: function () {
        return {
            count: 0
        };
    },
    onClick2: function () {
        this.setState({ count: this.state.count + 1 });
        var requestData = {
            count: this.state.count
        };

        console.log("AAAAAAAAAA")
        
        $.ajax({
            url: this.props.url,
            dataType: "json",
            type: "POST",
            data: JSON.stringify(requestData),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
            }
        });
    },
    render: function () {
        const divStyle = {
            color: 'red'
        };
        return (
            <div>
                <span>{this.state.count}</span>
                <button onClick={this.onClick2}>click</button>
            </div>
        );
    }
});

ReactDOM.render(<Test url="/Home/PostDate" />, document.getElementById('IamButton'));




//  注意元件開頭第一個字母都要大寫
class MyComponent extends React.Component {
    // render 是 Class based 元件唯一必須的方法（method）
    render() {
        const divStyle = {
            color: 'red'
        };
        return (            
            <div style = {divStyle}>Hello, World!</div>
        );
    }
}


// 將 <MyComponent /> 元件插入 id 為 app 的 DOM 元素中
ReactDOM.render(<MyComponent />, document.getElementById('testApp'));


class HelloMessage extends React.Component {
    // 若是需要綁定 this.方法或是需要在 constructor 使用 props，定義 state，就需要 constructor。若是在其他方法（如 render）使用 this.props 則不用一定要定義 constructor
    constructor(props) {
        // 對於 OOP 物件導向程式設計熟悉的讀者應該對於 constructor 建構子的使用不陌生，事實上它是 ES6 的語法糖，骨子裡還是 prototype based 物件導向程式語言。透過 extends 可以繼承 React.Component 父類別。super 方法可以呼叫繼承父類別的建構子
        super(props);
        this.state = {}
    }
    // render 是唯一必須的方法，但如果是單純 render UI 建議使用 Functional Component 寫法，效能較佳且較簡潔
    render() {
        return (
            <div>Hello {this.props.name}</div>
        )
    }
}

// PropTypes 驗證，若傳入的 props type 不是 string 將會顯示錯誤
HelloMessage.propTypes = {
    name: React.PropTypes.string,
}

// Prop 預設值，若對應 props 沒傳入值將會使用 default 值 Zuck
HelloMessage.defaultProps = {
    name: 'Zuck',
}

ReactDOM.render(<HelloMessage name="Mark" />, document.getElementById('testApp2'));

const lists = ['JavaScript', 'Java', 'Node', 'Python'];

class HelloMessage1 extends React.Component {
    render() {
        return (
            <div>
                <span>1111111</span>
                <ul>
                    {lists.map((result, index) => {
                        console.log(index + ", " + result);
                        return (<li key={index}>{result}</li>);
                    })}
                </ul>
            </div>
        );
    }
}

ReactDOM.render(<HelloMessage1 />, document.getElementById('testApp3'));

class Timer extends React.Component {
    constructor(props) {
        super(props);
        // 與 ES5 React.createClass({}) 不同的是 component 內自定義的方法需要自行綁定 this context，或是使用 arrow function
        this.tick = this.tick.bind(this);
        // 初始 state，等於 ES5 中的 getInitialState
        this.state = {
            secondsElapsed: 0,
        }
    }
    // 累加器方法，每一秒被呼叫後就會使用 setState() 更新內部 state，讓 Component 重新 render
    tick() {
        this.setState({ secondsElapsed: this.state.secondsElapsed + 1 });
    }
    // componentDidMount 為 component 生命週期中階段 component 已插入節點的階段，通常一些非同步操作都會放置在這個階段。這便是每1秒鐘會去呼叫 tick 方法
    componentDidMount() {
        this.interval = setInterval(this.tick, 1000);
    }
    // componentWillUnmount 為 component 生命週期中 component 即將移出插入的節點的階段。這邊移除了 setInterval 效力
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    // render 為 class Component 中唯一需要定義的方法，其回傳 component 欲顯示的內容
    render() {
        return (
            <div>Seconds Elapsed: {this.state.secondsElapsed}</div>
        );
    }
}

ReactDOM.render(<Timer />, document.getElementById('testApp4'));



class TestApp5 extends React.Component {
    constructor(props) {
        super(props);
        console.log('constructor');
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            name: 'Mark',
        }
    }
    handleClick() {
        this.setState({ 'name': 'Zuck' });
    }
    componentWillMount() {
        console.log('componentWillMount');
    }
    componentDidMount() {
        
        console.log('componentDidMount');
    }
    componentWillReceiveProps() {
        console.log('componentWillReceiveProps');
    }
    componentWillUpdate() {
        console.log('componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }
    render() {
        return (
            <div onClick={this.handleClick}>Hi, {this.state.name}</div>
        );
    }
}

ReactDOM.render(<TestApp5 />, document.getElementById('testApp5'));

class TestApp6 extends React.Component {    
    constructor(props) {
        super(props);        
        console.log('constructor');
        this.onClick2 = this.onClick2.bind(this);
        this.tick = this.tick.bind(this);
        this.state = {
            timer: 0,
            timerSwitch: 1,
            name:"",
            data: ['bob', 'chris','john'],
        }
    }
    tick() {
        this.setState({ timer: this.state.timer + 1 });
    }
    onClick2() {
        if (this.state.timerSwitch == 1) {
            console.log("true")
            this.setState({ timerSwitch: 0 });
            this.interval = setInterval(this.tick, 1000);            
        } else {
            console.log("false")
            this.setState({ timerSwitch: 1 });
            clearInterval(this.interval);
        }
    }
    componentWillMount() {
        console.log('componentWillMount');
    }
    componentDidMount() {
        console.log('componentDidMount');
    }
    componentWillReceiveProps() {
        console.log('componentWillReceiveProps');
    }
    componentWillUpdate() {
        if (this.state.timer % 3 == 0) {
            this.setState({ name: this.state.data[0] });
        } else if (this.state.timer % 3 == 1) {
            this.setState({ name: this.state.data[1]});
        } else {
            this.setState({ name: this.state.data[2]});
        }
        console.log('componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }
    render() {
        return (
            <div>
                <div>[{this.state.timer}:{this.state.name}] <button onClick={this.onClick2}>click</button></div>
            </div>
        );
    }
}

ReactDOM.render(<TestApp6 />, document.getElementById('testApp6'));

