﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Diagnostics;

namespace HelloWorldReact.Controllers
{

    public class HomeController : Controller
    {
        public class NumberCount
        {
            public string Count { get; set; }
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetDate()
        {
            //Debug.WriteLine("Send to debug output.--->" + DateTime.Now.ToString("yyyy-MM-dd"));
            return Json(DateTime.Now.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult PostDate(NumberCount buttonClick)
        {
            Debug.WriteLine("buttonClick.--->" + buttonClick.Count);
            return Json(buttonClick.Count, JsonRequestBehavior.DenyGet);
        }
    }
}