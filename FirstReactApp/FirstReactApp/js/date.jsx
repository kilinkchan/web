﻿var App2 = React.createClass({
    getInitialState: function () {
        return { data: '', test: 'initAAA' };
    },
    getDefaultProps: function () {
        console.log('SSSSSS')
        return { txt: 'this is a prop', cat: 'Hello Morris' };
    },
    updateTest: function (e) {
        console.log('WWWWWWW-->' + e)
        this.setState({ test: e.target.value });
    },
    componentWillMount: function () {
        var xhr = new XMLHttpRequest();
        xhr.open('get', this.props.url, true);
        xhr.onload = function () {
            var response = JSON.parse(xhr.responseText);
            console.log("BBBBBBB-->" + response);
            this.setState({ data: response });
        }.bind(this);
        xhr.onerror = new function (e) {
            console.log("CCCCCC--->" + xhr.statusText);
        }
        xhr.send(null);
    },
    render: function () {
        console.log("AAAAAAAAAAA-->" + this.state.data);
        if (this.state.data) {
            return (
                <div>
                    <a href="https://facebook.github.io/react">React</a>
                    <h1>{this.state.data}</h1>
                    <p>{this.props.txt}</p>
                    <p>{this.props.cat}</p>
                    <p>{this.state.test}</p>
                    <div>XXXXXXXXXXXXxx</div>
                    <input type="text" onChange={this.updateTest}></input>
                </div>
            );
        } else {
            return (
                <h1>Loading ... </h1>
            );
        }
    }
})
ReactDOM.render(<App2 url="/Home/GetDate" />, document.getElementById('divDate'));
ReactDOM.render(
    <h1>Hello, world!</h1>,
    document.getElementById('divTest')
);